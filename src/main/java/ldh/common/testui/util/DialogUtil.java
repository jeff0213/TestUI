package ldh.common.testui.util;

import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import ldh.fx.ui.util.StageMovable;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by ldh on 2018/3/20.
 */
public class DialogUtil {

    public static void alert(String content, Alert.AlertType type) {
        String title = getAlertTypeTitle(type);
        Label contentLabel = new Label(content);
        JFXAlert<Void> jfxAlert = createModelDialog(contentLabel, title, ((jfxAlert1, jfxDialogLayout) -> {return null;}));
        jfxAlert.show();
    }

    public static void confirm(String content, Consumer<?> consumer) {
        Window owner = UiUtil.STAGE;
        Alert dlg = new Alert(Alert.AlertType.CONFIRMATION, "");
        dlg.initOwner(owner);
        dlg.setContentText(content);
        Optional<ButtonType> result = dlg.showAndWait();
        if (result.get() == ButtonType.OK){
            consumer.accept(null);
        }
    }

    public static JFXDialog createDialog(Node contentPane, String title, BiFunction<JFXDialog, JFXDialogLayout, List<JFXButton>> function) {
        JFXDialog dialog = new JFXDialog();
//        dialog.setTransitionType(JFXDialog.DialogTransition.TOP);
        dialog.setOverlayClose(false);
        JFXDialogLayout layout = new JFXDialogLayout();

        layout.setHeading(new Label(title));
        layout.setBody(contentPane);

        if (function != null) {
            List<JFXButton> jfxButtons = function.apply(dialog, layout);
            if (jfxButtons != null) {
                layout.getActions().addAll(jfxButtons);
            }
        }

        JFXButton closeButton = new JFXButton("关闭");
        closeButton.setOnAction(event -> dialog.close());
        layout.getActions().addAll(closeButton);

        dialog.setContent(layout);
        dialog.setDialogContainer(UiUtil.ROOT_PANE);

//        new ReginMovable((Region) layout.getParent());
        return dialog;
    }

    public static <R>JFXAlert<R> createModelDialog(Node contentPane, String title, BiFunction<JFXAlert, JFXDialogLayout, List<JFXButton>> function) {
        Window owner = UiUtil.STAGE;
        JFXAlert alert = new JFXAlert(owner);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initStyle(StageStyle.TRANSPARENT);
        alert.setOverlayClose(false);

        alert.getDialogPane().setStyle("-fx-background-color: transparent");

        JFXDialogLayout layout = new JFXDialogLayout();
        layout.setHeading(new Label(title));
        layout.setBody(contentPane);

        if (function != null) {
            List<JFXButton> jfxButtons = function.apply(alert, layout);
            if (jfxButtons != null) {
                layout.getActions().addAll(jfxButtons);
            }
        }

        JFXButton closeButton = new JFXButton("关闭");
        closeButton.setOnAction(event -> alert.close());
        layout.getActions().addAll(closeButton);

        alert.setContent(layout);

        new StageMovable(layout);

//        layout.getParent().setStyle("-fx-background-color: blue");
        layout.getScene().setFill(Color.TRANSPARENT);
        return alert;
    }

    public static String getAlertTypeTitle(Alert.AlertType alertType) {
        if (alertType == null) return "";
        if (alertType == Alert.AlertType.CONFIRMATION) {
            return "请确认";
        } else if (alertType == Alert.AlertType.ERROR) {
            return "错误";
        } else if (alertType == Alert.AlertType.INFORMATION) {
            return "提示";
        } else if (alertType == Alert.AlertType.WARNING) {
            return "警告";
        }
        return "";
    }
}
