package ldh.common.testui.handle;

import ldh.common.testui.model.TreeNode;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class TreeContext {

    private Map<String, Object> paramMap = new HashMap();
    private TreeContext parent;
    private TreeNode treeNode;

    public TreeContext(TreeNode treeNode) {
        this.treeNode = treeNode;
    }

    public TreeContext child(TreeNode treeNode) {
        TreeContext childTreeContext = new TreeContext(treeNode);
        childTreeContext.setParent(this);
        Map<String, Object> newParamMap = new HashMap<>();
        newParamMap.putAll(paramMap);
        childTreeContext.setParamMap(newParamMap);
        return childTreeContext;
    }

    public void addParams(Map<String, Object> newParamMap) {
        paramMap.putAll(newParamMap);
    }

    public void addParam(String key, Object value) {
        paramMap.put(key, value);
    }
}
