package ui;

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import ldh.common.testui.TestUIMainApp;
import ldh.common.testui.cell.ObjectTreeCell;
import ldh.common.testui.constant.TreeNodeType;
import ldh.common.testui.model.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ldh on 2018/3/27.
 */
public class TreeViewTest4 extends Application{

    public final TreeItem<String> treeRoot = new TreeItem("root");

    private List<TreeItem<String>> defaultSelected = new ArrayList();

    @Override
    public void start(Stage primaryStage) throws Exception {
        TreeView<String> treeView = new TreeView();
        treeView.setRoot(treeRoot);
        treeView.setShowRoot(true);
        treeRoot.setExpanded(true);
        treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        treeView.setOnMouseClicked(e->{
            System.out.println("click!!!!!!!!!!!");
            if (treeView.getSelectionModel().getSelectedItems().size() > 0) {
                for (TreeItem<String> item : defaultSelected) {
                    treeView.getSelectionModel().select(item);
                }
            }
        });

        loadData(treeView);
        VBox vBox = new VBox();
        vBox.getStyleClass().add("root-pane");
//        BackgroundImage myBI= new BackgroundImage(new Image("/bg4.jpg"),
//                BackgroundRepeat.SPACE, BackgroundRepeat.SPACE, BackgroundPosition.DEFAULT,
//                BackgroundSize.DEFAULT);
////then you set to your node
//        vBox.setBackground(new Background(myBI));
        JFXDecorator jfxDecorator = new JFXDecorator(primaryStage, vBox);
        HBox hBox = new HBox();
        Button b = new Button("show style");
        b.setOnAction(e->System.out.println("style: " + jfxDecorator.getStyle()));
        hBox.getChildren().add(b);
        vBox.getChildren().addAll(hBox, treeView);

//        Scene scene = new Scene(jfxDecorator, 1200, 600);
        Scene scene = new Scene(jfxDecorator, 600, 300);
        scene.setFill(Color.TRANSPARENT);
        scene.getStylesheets().add(TestUIMainApp.class.getResource("/css/tree4.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();

//        System.out.println("style: " + jfxDecorator.getStyle());
    }

    private void loadData(TreeView<String> treeView) {
        for (int i=0; i<5; i++) {
            TreeItem<String> treeItem = new TreeItem<>("test" + i);
            if (i == 1 || i==3) {
                defaultSelected.add(treeItem);
            }
            treeRoot.getChildren().add(treeItem);
            treeItem.setExpanded(true);
            initChild(treeItem);
        }
    }

    private void initChild(TreeItem<String> treeItem) {
        for (int i=0; i<5; i++) {
            TreeItem<String> treeItem2 = new TreeItem<>("child" + i);
            treeItem.getChildren().add(treeItem2);
        }
    }

    public static void main(String[] args) throws Exception {
        Application.launch(args);
    }
}
