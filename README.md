# TestUI
       TestUi是一款javafx开发测试框架，能够测试Http接口，以及Java方法测试。主要是通过一个简单  
UI界面，让产品，测试，开发对各自关注的测试点，进行测试，降低测试难度。  
       测试有三大步： 数据的准备，功能触发，数据的验证：  
       功能触发一般最为简单，目前支持的工具  也最多，如： postman, jmeter。而数据的准备和数据的  
验证是测试环境中最为繁琐，重复工作很多，尤其当测试用例复杂时，重复工作尤为严重。针对这种情况，  
本工具应运而生，在数据准备环节，提供两种准备数据方式： 一种是准备什么数据，就调用相应的接口或方法，    
直到数据准备完整。另外一种方式，在数据准备完整后直接对数据进行快照并导出，再次测试时，直接将数据导入，  
这种方法最好配合内存数据库使用，一直准备，多次使用。在数据的验证环节，直接增加了对方法的返回值以及  
接口的返回值进行验证，并且还能对数据库中数据进行验证，省事省力。  
       本测试工具，还有许多功能，解决测试的痛点，如： 动态变量，函数， debug模式运行测试用例，运行记录等。    
动态变量能够在每次测试时，生成不一样的数据，能够集成测试。
    
    主要功能如下：
      1，  自定义变量  
      2，  支持变量运算 
      3，  测试Http接口
      4，  验证http返回结果值，支持json格式验证
      5，  测试Java方法
      6，  验证调用Java方法后的数据，支持Java对象的验证
      7，  支持数据库中数据验证（当前仅支持Mysql, h2数据库）
      8，  对测试结果进行保存，生成测试报表
      9，  支持debug模式运行测试功能
      10， 支持swagger接口导入
      11， 支持数据的备份和导入
      12， 测试用例前数据的导入（未完成）
      13， 测试用例完数据的清理（未完成）
      
## 界面示意图： 
参数  
![Alt text](https://gitee.com/ldh123/TestUI/raw/master/doc/images/测试参数变量.png)  
 测试Java方法  
![Alt text](https://gitee.com/ldh123/TestUI/raw/master/doc/images/java方法测试.png)  
 验证返回值  
![Alt text](https://gitee.com/ldh123/TestUI/raw/master/doc/images/验证返回值.png)  
验证结果（el表达是验证法）  
![Alt text](https://gitee.com/ldh123/TestUI/raw/master/doc/images/el表达式验证对象.png)  
验证接口返回值
![Alt text](https://gitee.com/ldh123/TestUI/raw/master/doc/images/验证接口返回值.png)  
验证结果（json验证）
![Alt text](https://gitee.com/ldh123/TestUI/raw/master/doc/images/复杂json对象验证.png) 
swagger导入
![Alt text](https://gitee.com/ldh123/TestUI/raw/master/doc/images/swagger导入.png) 
交流群  
![Alt text](https://gitee.com/ldh123/TestUI/raw/master/doc/images/testui群二维码.png)   


